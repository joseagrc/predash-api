
# PREDASH API

This guide show how config Predash in server


## Get Started

### Configuration Flask Tryton enviroment

#### Create a config file

In Home you can create a new file for example in '/home/user/.flask':


```
nano /home/user/.flask/env_flask.ini

```

#### Add General Section information

Add all info required.

```
[General]
databases=['MYDB']
host=0.0.0.0
trytond_config=/home/user/.trytond/trytond.conf
```

#### Add Auth Section (Optional)

Add api_key and secret section if you want share API with parties.

```
[Auth]
api_key=XXXXXXXXXXXXXXXXXXXXXXX
secret_key=mysupersecretkey
user=admin
```


#### Activate SSL (Optional)

Creating Private Key and Certificate, so in console type:

```
openssl req -x509 -newkey rsa:4096 -keyout key.pem -out cert.pem -days 365

```

Next add this section:

```
[SSL]
cert_file = /home/user/.flask/cert.pem
key_file = /home/user/.flask/key.pem
```


## Endpoints


## Test Running server using uWSGI

gunicorn -c gun_config.py wsgi:app
