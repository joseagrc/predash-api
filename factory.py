import logging
import base64
import types
import hmac
import hashlib
from datetime import date
import simplejson as json
from flask import Flask, request
from flask_tryton import Tryton
from flask.json import JSONEncoder
from models import MODELS
from flask_cors import CORS
from tools import (send_data, get_config, prepare_ctx_view, get_webfields,
    get_webuserfom)


trytond_config = get_config('trytond_config')
SECRET = 'e8c80f3711d62a0b600de821941989d0565fe5874a8c8c3694c8d19dd52ab049'


def verify_webhook_shopify(data, hmac_header):
    digest = hmac.new(SECRET.encode('utf-8'), data, hashlib.sha256).digest()
    genHmac = base64.b64encode(digest)
    return hmac.compare_digest(genHmac, hmac_header.encode('utf-8'))


def is_class(obj):
    if obj.get('__class__'):
        # FIXME: Add datetime, bytes, decimal, too
        if obj['__class__'] == 'date':
            return date(obj['year'], obj['month'], obj['day'])
    return obj


def get_data(decode=False):
    if request.args:
        # From react web
        data = request.args.to_dict()
        if data.get('context'):
            data['context'] = eval(data['context'])
        return data
    elif hasattr(request, 'data') and request.data != b'':
        data = request.data.decode("utf-8")
        return json.loads(data, object_hook=is_class)
    else:
        try:
            return request.json
        except AttributeError:
            logging.warning('Attribute error unknown...!')


class CustomJSONEncoder(JSONEncoder):

    def default(self, obj):
        try:
            if isinstance(obj, types.ModuleType):
                return str(obj)
            elif isinstance(obj, types.MethodType):
                return str(obj)
            elif isinstance(obj, date):
                return obj.isoformat()
            elif isinstance(obj, 'bytes'):
                return base64.b64encode(obj)
            iterable = iter(obj)
        except TypeError:
            pass
        else:
            return list(iterable)
        return JSONEncoder.default(self, obj)


def create_app(dbname):
    app = Flask(dbname, instance_relative_config=True)
    CORS(app)
    app.json_encoder = CustomJSONEncoder
    app.config['TRYTON_DATABASE'] = dbname
    app.config['TRYTON_CONFIG'] = trytond_config
    app.config['CORS_HEADERS'] = 'Content-Type'
    try:
        tryton = Tryton(app)
    except:
        logging.warning('Error database disabled or unknown error: %s' % dbname)
        return None
    _pool = tryton.pool
    _models = MODELS.copy()

    @tryton.transaction()
    def clear_fields_models():
        # Remove fields from modules not activated
        ModelField = _pool.get('ir.model.field')

        for model, values in _models.items():
            active_fields = ModelField.search_read(
                [('model.model', '=', model)], fields_names=['name']
            )

            active_fields = [af['name'] for af in active_fields]
            for vf in values['fields']:
                fields_rec = vf.split('.')
                if fields_rec[0] not in active_fields:
                    _models[model]['fields'].remove(vf)

    @app.route('/')
    @tryton.transaction()
    def home():
        Company = _pool.get('company.company')
        company, = Company.search_read([
            ('id', '=', 1)
        ], fields_names=['party.name'])

        msg = 'Hello welcome to %s' % company['party.name']
        return send_data(msg)

    @app.route('/login', methods=['GET'])
    @tryton.transaction()
    def login():
        res = {'user': None}
        user = request.args.get('user', None)
        passwd = request.args.get('passwd', None)

        User = _pool.get('res.user')
        users = User.search([
            ('login', '=', user),
            ('active', '=', True),
        ])

        # FIXME: Add check password ('password', '=', passwd),
        if users:
            _user = users[0]
            # res['token'] = encrypt(SECRET_KEY, user).decode("utf-8")
            res['token'] = 'not token'
            res['user'] = _user.id
            res['user_name'] = _user.name
            res['company'] = _user.company.id
            res['company_name'] = _user.company.party.name
            res['currency'] = _user.company.currency.id
            res['timezone'] = _user.company.timezone
            if hasattr(_user, 'shop'):
                res['shop'] = _user.shop.id
                res['shop_name'] = _user.shop.name

        return send_data(res)

    @app.route("/search", methods=['POST'])
    def search():
        data = get_data()
        model = data.get('model', None)
        order = data.get('order', None)
        domain = data.get('domain', [])
        context = data.get('context', {})
        limit = data.get('limit', None)
        fields = data.get('fields', None)
        if isinstance(domain, str):
            try:
                domain = eval(domain)
            except:
                domain = []

        @tryton.transaction(context=context)
        def _search():
            Model = _pool.get(model)
            fields_names = fields
            bin = None
            if _models.get(model):
                bin = _models[model].get('binaries')
                if not fields:
                    fields_names = _models[model]['fields']
            records = Model.search(domain, limit=limit, order=order)
            return [jsonify_record(rec, fields_names, bin) for rec in records]

        records = _search()
        return send_data(records)

    @app.route("/delete", methods=['DELETE'])
    @tryton.transaction()
    def delete():
        data = get_data()
        model = data.get('model', None)
        ids = data.get('ids', [])
        Model = _pool.get(model)
        records = Model.browse(ids)
        res = Model.delete(records)
        return send_data(res)

    @app.route("/translations", methods=['GET'])
    @tryton.transaction()
    def translations():

        ModelLang = _pool.get('dash.model.lang')
        langs = ModelLang.search([])
        res = {}
        for l in langs:
            model_name = l.dash_model.model.model
            if not res.get(l.lang.code):
                res[l.lang.code] = {}
            res[l.lang.code][model_name] = l.dash_model.model.name.upper()

            for t in l.translations:
                name_ = 'model.' + model_name + '.' + t.name
                res[l.lang.code][name_] = t.value
        return send_data(res)

    @app.route("/model", methods=['GET'])
    @tryton.transaction()
    def model():
        data = get_data()
        model_name = data.get('name', None)
        Model = _pool.get('dash.model')
        records = Model.search([
            ('model.model', '=', model_name)
        ])
        model_ctx = {}
        if records:
            model_ctx = prepare_ctx_view(records[0])

        return send_data(model_ctx)

    @app.route("/webform", methods=['GET'])
    @tryton.transaction()
    def webform():
        model_name = request.args.get('model', None)

        DashModel = _pool.get('dash.model')
        records = DashModel.search([
            ('model.model', '=', model_name)
        ])

        model_ctx = {}
        if records:
            dash_model = records[0]
            target_model = _pool.get(model_name)
            webfields, required = get_webfields(dash_model, target_model)
            model_ctx['webfields'] = webfields
            model_ctx['required'] = required
            model_ctx['webuserform'] = get_webuserfom(dash_model.webuserform)

        return send_data(model_ctx)

    @app.route("/models", methods=['GET'])
    @tryton.transaction()
    def models():
        user = request.args.get('user', None)
        Access = _pool.get('dash.access')
        records = Access.search([
            ('user', '=', int(user))
        ])
        models = []
        for rec in records:
            for dm in rec.access_models:
                target_model = _pool.get(dm.model.model)
                models.append(prepare_ctx_view(dm, target_model))

        return send_data(models)

    @app.route("/notification/shopify", methods=['POST'])
    @tryton.transaction()
    def notification_shopify():
        data = request.get_data()
        header = request.headers.get('X-Shopify-Topic')
        verified = verify_webhook_shopify(data, request.headers.get('X-Shopify-Hmac-SHA256'))
        if verified:
            req = request.data.decode("utf-8")
            data = json.loads(req)
            if not data:
                return send_data([{}])
            Channel = _pool.get('sale.web_channel.shopify')
            res = Channel.request_api(data, header)
            return '...', 200

    @app.route("/notification/mercadolibre", methods=['POST'])
    @tryton.transaction()
    def notification_mercado_libre():
        req = request.data.decode("utf-8")
        data = json.loads(req)
        if not data:
            return send_data([{}])

        def create_sale(data):
            Channel = _pool.get('sale.web_channel.mercado_libre')
            error = ''
            try:
                res = Channel.request_api(data)
            except Exception as error:
                print('Caught this error: ' + repr(error))
                channel_ = Channel._get_channel()
                channel_.send_mail_notification(error.message + ' in resource \n' + str(data))
                return {'error': 'Fail process in server'}
            return res

        res = create_sale(data)
        return send_data(res)

    @app.route("/notifications", methods=['POST'])
    @tryton.transaction()
    def notifications():
        req = request.data.decode("utf-8")
        data = json.loads(req)
        if not data:
            return send_data([{}])
        # response = [{'status': 'ok', 'msg': 'Successfull Process !!!'}]

        def _get_user():
            User = _pool.get('res.user')
            user, = User.search([
                ('login', '=', 'mercado.libre')
            ])
            return user

        user_ = _get_user()
        shop_id, user_id = user_.shop.id, user_.id
        ctx = {'company': user_.company.id, 'user': user_id}

        # @tryton.transaction(context=ctx)
        def create_sale(data):
            Channel = _pool.get('sale.web_channel')
            res = Channel.create_sale_api(data)
            return res

        res = create_sale(data)

        return send_data(res)

    @app.route("/search_selection", methods=['GET'])
    @tryton.transaction()
    def search_selection():
        data = get_data()
        model = data.get('model', None)
        domain = data.get('domain', None)
        Model = _pool.get(model)
        _domain = []
        _records = []

        if domain != 'false':
            if domain == 'undefined':
                _domain = []
            else:
                _domain = eval(domain)

            records = Model.search(_domain)

            if _models.get(model):
                fields_names = _models[model]['fields']
            else:
                fields_names = ['id', 'name']

            _records = [jsonify_record(r, fields_names) for r in records]
        return send_data(_records)

    def convert_type(val):
        if isinstance(val, float):
            val = str(val)
        return val

    def jsonify_record(record, fields=None, binaries=None):
        res_fields = {}
        if not fields:
            fields = ['id', 'name', 'rec_name']
        else:
            fields.append('rec_name')
        for fd in fields:
            if '.' in fd:
                target = fd.split('.')
                if target[0] not in res_fields.keys():
                    res_fields[target[0]] = []
                res_fields[target[0]].append(target[1])
            else:
                res_fields[fd] = []

        _record_data = {'id': record.id}

        for f in res_fields:
            value = getattr(record, f, None)
            # try:
            # except:
            #     logging.warning('Attribute field unknown > ', f, record.__name__)
            #     _record_data[f] = None
            #     continue

            if value is None:
                _record_data[f] = None
                continue
            elif hasattr(value, 'id'):
                _record_data[f] = {
                    'id': value.id
                }
                if hasattr(value, 'rec_name'):
                    _record_data[f]['name'] = value.rec_name
                elif hasattr(value, 'name'):
                    _record_data[f]['name'] = value.name

                child_fields = res_fields.get(f)
                if child_fields:
                    _record_data[f] = jsonify_record(value, child_fields)
            elif value and isinstance(value, tuple):
                try:
                    sub_model = value[0].__name__
                    sub_fields = _models[sub_model]['fields']
                    sub_elements = [(jsonify_record(e, sub_fields)) for e in value]
                    _record_data[f] = sub_elements
                except:
                    pass
            elif value and isinstance(value, list):
                # Flask require that lists are dumped before send
                _record_data[f] = json.dumps(value)
            else:
                _record_data[f] = value

        if binaries:
            for fb in binaries:
                val = getattr(record, fb, None)
                if val:
                    _record_data[fb] = base64.b64encode(val)
                else:
                    _record_data[fb] = None
        return _record_data

    @app.route("/create", methods=['POST'])
    def create():
        data = json.loads(request.data.decode("utf-8"))
        model = data['model']
        rec = data['record']
        ctx = data.get('context', {})
        msg = {
            'status': 'error',
            'info': {}
        }

        to_create = {}
        for key, val in rec.items():
            if key == 'id' or not val:
                continue
            if isinstance(val, dict):
                to_create[key] = val.get('id')
            elif isinstance(val, list):
                subvalues = []
                for v in val:
                    to_add = {}
                    if v.get('id'):
                        _ = v.pop('id')
                    for k, d in v.items():
                        if isinstance(d, dict):
                            to_add[k] = d.get('id')
                        else:
                            d = convert_type(d)
                            to_add[k] = d
                    subvalues.append(to_add)
                to_create[key] = [('create', subvalues)]
            else:
                val = convert_type(val)
                to_create[key] = val

        user = None
        if ctx:
            user = ctx.get('user')
        else:
            # In web forms there isn't context, we need to keep from
            # to search webuser in database
            # User = _pool.get('res.user')
            try:
                # webuser, = User.search([
                #     ('login', '=', 'webuser')
                # ])
                # ctx = {'company': webuser.company.id, 'user': webuser.id}
                # user = webuser.id
                ctx = {'company': 1, 'user': 1}
                user = 1
            except:
                logging.warning('Missing web user...!')

        @tryton.transaction(context=ctx, user=user)
        def _create():
            Model = _pool.get(model)
            _record, = Model.create([to_create])
            _fields = _models[model]['fields']
            return jsonify_record(_record, _fields)

        new_rec = _create()
        msg['status'] = 'ok'
        return send_data(new_rec)

    @app.route("/save", methods=['PUT'])
    def save():
        data = get_data(decode=True)
        if data.get('ids'):
            record_ids = data.get('ids')
        else:
            record_ids = [str(data['id'])]

        model = data['model']
        record_data = data['record_data']
        fields = data.get('fields', None)
        ctx = data.get('context', {})
        user = ctx.get('user', 0)
        msg = {
            'status': 'error',
            'info': {}
        }

        @tryton.transaction(context=ctx, user=user)
        def _save():
            Model = _pool.get(model)
            records = Model.browse(record_ids)

            if not record_data:
                return {}

            for key, values in record_data.items():
                if values and isinstance(values, list):
                    to_write = []
                    for vl in values:
                        for k, j in vl.items():
                            if isinstance(j, dict):
                                vl[k] = j['id']
                        if vl.get('id') > 0:
                            to_write.append(
                                ('write', [vl['id']], vl)
                            )
                        else:
                            to_write.append(
                                ('create', [vl])
                            )
                    record_data[key] = to_write
                elif isinstance(values, dict):
                    record_data[key] = values.get('id')

            Model.write(records, record_data)

            _fields = fields
            if not _fields:
                _fields = _models[model]['fields']
            _recs = Model.browse(record_ids)
            res = []
            for r in _recs:
                res.append(jsonify_record(r, _fields))
            if len(res) == 1:
                res = res[0]
            return res

        rec = _save()
        msg['status'] = 'ok'
        return send_data(rec)

    @app.route("/action", methods=['PUT'])
    def action():
        data = json.loads(request.data.decode("utf-8"))
        model = data['model']
        action = data['action']
        ctx = data['context']
        record_data = data['record']
        record_id = record_data.pop('id')

        @tryton.transaction(context=ctx, user=ctx['user'])
        def execute_action():
            Model = _pool.get(model)
            record, = Model.browse([record_id])
            if record_data:
                for key, values in record_data.items():
                    if isinstance(values, dict):
                        to_write = []
                        for k, v in values.items():
                            # k is id of record
                            to_write.append(('write', [k], v))
                            record_data[key] = to_write
                Model.write([record], record_data)

            method_action = getattr(Model, action)
            res = method_action([record])
            fields_names = _models[model]['fields']
            _recs = Model.search([('id', '=', record_id)])
            rec = jsonify_record(_recs[0], fields_names)
            if not res:
                res = {}
            res['record'] = rec
            return res

        res = execute_action()
        return send_data(res)

    @app.route("/dash_reports", methods=['GET'])
    @tryton.transaction()
    def dash_reports():
        data = get_data()
        user = data.get('user', None)
        Access = _pool.get('dash.access')
        DashReport = _pool.get('dash.report')
        accs = Access.search([
            ('user', '=', int(user))
        ])
        _reports_ids = [r.id for acc in accs for r in acc.access_reports]
        _reports = DashReport.search_read([
            ('id', 'in', _reports_ids)
        ], fields_names=['name', 'method', 'comment', 'type', 'in_thousands'])

        return send_data(_reports)

    @app.route("/report", methods=['POST'])
    @tryton.transaction()
    def report():
        data = get_data()
        ctx = data.get('context')
        report_name = data.get('report', None)
        args = data.get('args', None)
        report = _pool._pool[ctx['database']]['report'][report_name]
        oext, content, direct_print, name = report.execute([], args)
        res = {
            'name': name,
            'oext': oext,
            'content': base64.b64encode(content),
            'direct_print': direct_print,
        }
        return send_data(res)

    @app.route("/report_context", methods=['GET'])
    @tryton.transaction()
    def report_context():
        data = get_data()
        database = data.get('database', None)
        report_name = data.get('report_name', None)
        args = data.get('args', {})

        def report_colgaap(records, company_name):
            accounts = {}
            for rec in records:
                accounts[rec.code] = {
                    'code': rec.code,
                    'name': rec.name + ' ' + company_name,
                    'balance': rec.balance,
                }
            return accounts

        Report = _pool._pool[database]['report'][report_name]
        _records = Report.get_context(args['records'], args['data'])
        result = {}
        if report_name in [
            'account_col.balance_sheet_colgaap',
            'account_col.income_statement_colgaap']:
            company_name = _records['company'].rec_name
            result['records'] = report_colgaap(_records['records'], company_name)
            result['global_result'] = _records['global_result']
        elif report_name == 'account_col.trial_balance_classic':
            result = _records['accounts']
            print('RESULTADO ....', result)
        return send_data(result)

    @app.route("/report_data", methods=['GET'])
    @tryton.transaction()
    def report_data():
        data = get_data()
        report_id = data.get('id', None)
        ctx = data.get('context', {})

        _records = []
        DashReport = _pool.get('dash.report')
        dash_report, = DashReport.browse([report_id])
        data_ = {'report': dash_report}

        if dash_report.model:
            Model = _pool.get(dash_report.model.model)
            method = getattr(Model, dash_report.method)
            _records = method(data_, ctx)

        return send_data(_records)

    @app.route("/model_method", methods=['POST', 'GET'])
    def model_method():
        data = get_data()
        model = data.get('model', None)
        method = data.get('method', None)
        args = data.get('args', {})
        ctx = data.get('context', {})
        if isinstance(args, list) and args:
            args = args[0]
        elif isinstance(args, str) and args:
            args = json.loads(args)

        if isinstance(ctx, str) and ctx:
            ctx = json.loads(ctx)

        user = ctx.get('user') or 0

        @tryton.transaction(context=ctx, user=user)
        def _get_model_method():
            Model = _pool.get(model)
            _method = getattr(Model, method)
            if args:
                if isinstance(args, dict):
                    res = _method(args, ctx)
                else:
                    logging.warning('Using deprecation option, please fix it!')
                    res = _method(**args)
            else:
                res = _method()

            return res

        res = _get_model_method()
        return send_data(res)

    @app.route("/execute", methods=['POST'])
    def execute():
        """
        Execute a tryton model method
        """
        data = get_data()
        model = data.get('model', None)
        method = data.get('method', None)
        ctx = data.get('context', {})
        args = data.get('args', {})
        if isinstance(args, list) and args:
            args = args[0]
        elif isinstance(args, str) and args:
            args = json.loads(args)

        if isinstance(ctx, str) and ctx:
            ctx = json.loads(ctx)

        user = ctx.get('user') or 0

        @tryton.transaction(context=ctx, user=user)
        def _execute():
            Model = _pool.get(model)
            _method = getattr(Model, method)
            if args:
                if isinstance(args, dict):
                    res = _method(args, ctx)
                else:
                    logging.warning('Using deprecation option, please fix it!')
                    res = _method(**args)
            else:
                res = _method()
            return res

        res = _execute()
        return send_data(res)

    @app.route("/search_record", methods=['GET'])
    @tryton.transaction()
    def search_record():
        # Search a record using clause
        data = get_data()
        model = data.get('model', None)
        clause = data.get('clause', None)
        Model = _pool.get(model)
        _record = None
        if clause != 'false':
            _clause = [None, '=', clause]
            try:
                _domain = Model.search_rec_name(None, _clause)
                record, = Model.search(_domain)

                if _models.get(model):
                    fields_names = _models[model]['fields']
                else:
                    fields_names = ['id', 'name']

                _record = jsonify_record(record, fields_names)
            except:
                pass
        return send_data(_record)

    @app.route("/sheet", methods=['GET'])
    @tryton.transaction()
    def sheet():
        data = get_data()
        model = data.get('model', None)
        args = json.loads(data.get('args', None))
        ctx = {}
        Model = _pool.get(model)
        record_data = Model.get_sheet(args, ctx)
        return send_data(record_data)

    @app.route("/save_many", methods=['PUT'])
    def save_many():
        data = get_data(decode=True)
        model = data['model']
        values = data['values']
        ctx = data.get('context', {})
        user = ctx.get('user', 0)
        msg = {
            'status': 'ok',
            'info': {}
        }
        Model = _pool.get(model)

        @tryton.transaction(context=ctx, user=user)
        def _save_many():
            rec_ids = []
            for d in values:
                rec_ids.append(d.pop('id'))
            records = Model.browse(rec_ids)
            Model.write(records, d)

        _save_many()
        return send_data(msg)

    return app
